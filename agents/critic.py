from keras.models import Sequential, Model
from keras.layers import Dense, Input, Add
from keras.optimizers import Adam
import tensorflow as tf
import keras.backend as K

hiddenSize0 = 300
hiddenSize1 = 600

class Critic():
    def __init__(self, sess, stateSize, actionSize, t, lr):
        self.sess = sess
        self.t = t
        self.lr = lr
        self.actionSize = actionSize
        #setup for keras/tensorflow
        K.set_session(sess)

        #two models, one for the targets that will be used and updated less frequently for stability, one for the model that will be updated each time
        self.targetModel, self.targetAction, self.targetState = self.createNetwork(stateSize, actionSize)
        self.model, self.action, self.state = self.createNetwork(stateSize, actionSize)
        self.actionGradient = tf.gradients(self.model.output, self.action)
        self.sess.run(tf.initialize_all_variables())

    def gradients(self, states, actions):
        # print("STATES SHAPE: ")
        # print(states.shape)
        # print("ACTIONS SHAPE: ")
        # print(actions.shape)

        return self.sess.run(self.actionGradient, feed_dict={
            self.state: states,
            self.action: actions
        })[0]

    def targetTrain(self):
        #this function takes the trained system in actor weights and applies it across the target using Tau hyperparameter keeping it more stable
        criticWeights = self.model.get_weights()
        criticTargetWeights = self.targetModel.get_weights()

        for x in range(len(criticWeights)):
            criticTargetWeights[x] = self.t * criticWeights[x] + (1 - self.t)* criticTargetWeights[x]
        self.targetModel.set_weights(criticTargetWeights)
    
    def createNetwork(self, stateSize, actionDim):
        sInput = Input(shape=[stateSize])
        aInput = Input(shape=[actionDim], name='action')
        #splits for action and state networks that later get merged back into a value layer
        s0 = Dense(hiddenSize0, activation='relu')(sInput)
        a0 = Dense(hiddenSize1, activation='linear')(aInput)
        s1 = Dense(hiddenSize1, activation='linear')(s0)
        #merge
        h2 = Add()([s1,a0])
        h3 = Dense(hiddenSize1, activation='relu')(h2)
        value = Dense(actionDim, activation='linear')(h3)
        model = Model(input=[sInput,aInput], output=value)
        adam = Adam(lr=self.lr)
        model.compile(loss='mse', optimizer=adam)
        return model, aInput, sInput
    
