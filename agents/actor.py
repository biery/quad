from keras.models import Sequential, Model
from keras.layers import Dense, Input, merge, Lambda
from keras.optimizers import Adam
import tensorflow as tf
import keras.backend as K

hiddenSize0 = 300
hiddenSize1 = 600

class Actor():
    def __init__(self, sess, stateSize, actionSize, actionLow, actionHigh, actionRange, t, lr):
        self.sess = sess
        self.t = t
        self.actionLow = actionLow
        self.actionHigh = actionHigh
        self.actionRange = actionRange
        self.lr = lr
        #setup for keras/tensorflow
        K.set_session(sess)

        #two models, one for the targets that will be used and updated less frequently for stability, one for the model that will be updated each time
        self.targetModel, self.targetWeights, self.targetState = self.createNetwork(stateSize, actionSize)
        self.model, self.weights, self.state = self.createNetwork(stateSize, actionSize)

        self.actionGradient = tf.placeholder(tf.float32, [None, actionSize])
        self.paramsGradient = tf.gradients(self.model.output, self.weights, -self.actionGradient)
        gradients = zip(self.paramsGradient, self.weights)
        self.optimize = tf.train.AdamOptimizer(self.lr).apply_gradients(gradients)

        #initialize all variables for TF
        self.sess.run(tf.initialize_all_variables())

    def train(self, states, actionGradients):
        #normal tensorflow training method
        self.sess.run(self.optimize, feed_dict={self.state: states, self.actionGradient: actionGradients})
    
    def targetTrain(self):
        #this function takes the trained system in actor weights and applies it across the target using Tau hyperparameter keeping it more stable
        actorWeights = self.model.get_weights()
        actorTargetWeights = self.targetModel.get_weights()
        for x in range(len(actorWeights)):
            actorTargetWeights[x] = self.t * actorWeights[x] + (1 - self.t)* actorTargetWeights[x]
        self.targetModel.set_weights(actorTargetWeights)

    def createNetwork(self, stateSize, actionDim):
        sInput = Input(shape=[stateSize])
        #provide enough hidden layers to generalize function
        hiddenLayer0 = Dense(hiddenSize0, activation='relu')(sInput)
        hiddenLayer1 = Dense(hiddenSize1, activation='relu')(hiddenLayer0)
        #this should map the two hidden layers to an action map to make a decision
        action = Dense(actionDim, activation='sigmoid')(hiddenLayer1)
        scaleAction = Lambda(lambda x: (x * self.actionRange) + self.actionLow)(action)
        model = Model(input=sInput,output=scaleAction)
        return model, model.trainable_weights, sInput

