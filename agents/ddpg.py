import random
import numpy as np
import tensorflow as tf
import keras.backend as K
from agents.actor import Actor
from agents.critic import Critic
from collections import deque, namedtuple

#dx​t​​=θ(μ−x​t​​)dt+σdW​t​​

class OU():
    def function(self, x, mu, theta, sigma):
        return theta * (mu - x) + sigma * np.random.randn(1)

OU = OU()
TAU = 0.001
LRA = 0.0001
LRC = 0.001
BUFFER_SIZE = 1000000 
MINIBATCH_SIZE = 64
GAMMA = 0.99

class XPBuffer:
    def __init__(self):
        self.memory = deque(maxlen=BUFFER_SIZE)
        self.xp = namedtuple("xp", field_names=["state", "action", "reward", "next_state", "done"])

    def add(self, state, action, reward, next_state, done):
        e = self.xp(state, action, reward, next_state, done)
        self.memory.append(e)

    def sample(self, batch_size=64):
        return random.sample(self.memory, k=batch_size)

    def bufferLength(self):
        return len(self.memory)

class DDPG():
    def __init__(self,task):
        # Task (environment) information from original env
        self.task = task
        self.state_size = task.state_size
        self.action_size = task.action_size
        self.action_low = task.action_low
        self.action_high = task.action_high
        self.action_range = self.action_high - self.action_low

        sess = tf.Session()
        K.set_session(sess)

        print(self.action_size)

        # create our networks for actor and critic, each network has a model and target model
        self.actor = Actor(sess, self.state_size, self.action_size, self.action_low, self.action_high, self.action_range, TAU, LRA)
        self.critic = Critic(sess, self.state_size, self.action_size, TAU, LRC)

        #create noise using Ornstein-Uhlenbeck process
        self.mu = 0
        self.theta = 0.15
        self.sigma = 0.2
        self.noise = OU.function(self.action_size, self.mu, self.theta, self.sigma)

        #setup memory buffer and biggest reward for tracking
        self.memory = XPBuffer()
        self.biggest_total_reward = 0

    def reset_episode(self):
        self.total_reward = 0.0
        self.count = 0
        state = self.task.reset()
        self.last_state = state
        return state

    def step(self, action, reward, next_state, done):
        #add experience to memory
        self.memory.add(self.last_state, action, reward, next_state, done)
        
        #push rewards into total rewards and update biggest if needed
        self.total_reward += reward
        self.count += 1
        if self.total_reward > self.biggest_total_reward:
            self.biggest_total_reward = self.total_reward
        
        #sample out of past experiences if possible
        if self.memory.bufferLength() > MINIBATCH_SIZE:
            experiences = self.memory.sample()            
            self.learn(experiences)

        self.last_state = next_state

    def act(self, states):
        #will be used to generate prediction of next action in notebook
        state = np.reshape(states, [-1, self.state_size])
        action = self.actor.model.predict(state)[0]
        #add noise to returned action
        self.noise = OU.function(np.random.randn(self.action_size), self.mu, self.theta, self.sigma)
        return list(action + self.noise)


    def learn (self, experiences):
        #separate the batch into sections to be used with the training functions
        states = np.vstack([e.state for e in experiences if e is not None])
        actions = np.array([e.action for e in experiences if e is not None]).astype(np.float32).reshape(-1, self.action_size)
        rewards = np.array([e.reward for e in experiences if e is not None]).astype(np.float32).reshape(-1, 1)
        dones = np.array([e.done for e in experiences if e is not None]).astype(np.uint8).reshape(-1, 1)
        next_states = np.vstack([e.next_state for e in experiences if e is not None])

        #get actions based on the next state values
        actions_next = self.actor.targetModel.predict_on_batch(next_states)
        Q_targets_next = self.critic.targetModel.predict_on_batch([next_states, actions_next])

        #calculate current actions
        Q_targets = rewards + GAMMA * Q_targets_next * (1 - dones)
        self.critic.model.train_on_batch(x=[states, actions], y=Q_targets)

        actionsForGradients = self.actor.model.predict(states)
        gradients = self.critic.gradients(states, actionsForGradients)

        #train actor based on the critic network
        self.actor.train(states, gradients)

        #update both target networks
        self.critic.targetTrain()
        self.actor.targetTrain()

    def saveWeights(self):
        self.actor.model.save_weights("actor.h5", overwrite=True)
        self.critic.model.save_weights("actor.h5", overwrite=True)








    